package nl.devoxx4kids.devoxxmod.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import nl.devoxx4kids.devoxxmod.DevoxxMod;

public class BaseItem extends Item {

    protected String name;

    public BaseItem(String name) {
        this.name = name;
        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerItemModel(Item item) {
        DevoxxMod.proxy.registerItemRenderer(item, 0, name);
    }

    @Override
    public BaseItem setCreativeTab(CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }
}
