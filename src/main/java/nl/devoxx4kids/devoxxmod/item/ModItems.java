package nl.devoxx4kids.devoxxmod.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {

    public static BaseItem blueRedstone;

    public static void init() {
        blueRedstone = register(new BaseItem("blueRedstone").setCreativeTab(CreativeTabs.REDSTONE));
    }

    private static <T extends Item> T register(T item) {
        GameRegistry.register(item);

        if (item instanceof BaseItem) {
            ((BaseItem) item).registerItemModel(item);
        }

        return item;
    }
}
