package nl.devoxx4kids.devoxxmod;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ChatItems {

    @SubscribeEvent
    public void giveItems(ServerChatEvent event) {
        if(event.getMessage().contains("diamant")) {
            event.getPlayer().inventory.addItemStackToInventory(new ItemStack(Items.DIAMOND, 64));
        }
    }
}
