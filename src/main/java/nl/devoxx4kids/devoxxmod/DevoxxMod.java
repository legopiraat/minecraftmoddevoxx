package nl.devoxx4kids.devoxxmod;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import nl.devoxx4kids.devoxxmod.item.ModItems;
import nl.devoxx4kids.devoxxmod.proxy.CommonProxy;

@Mod(modid = DevoxxMod.MODID, name = DevoxxMod.NAME, version = DevoxxMod.VERSION, acceptedMinecraftVersions = "[1.10.2]")
public class DevoxxMod {

    public static final String MODID = "devoxxmod";
    public static final String NAME = "Devoxx4Kids Mod";
    public static final String VERSION = "1.0.0";

    @Mod.Instance(MODID)
    public static DevoxxMod instance;

    @SidedProxy(serverSide = "nl.devoxx4kids.devoxxmod.proxy.CommonProxy", clientSide = "nl.devoxx4kids.devoxxmod.proxy.ClientProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        System.out.println(NAME + "is aan het laden!");
        ModItems.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new ChatItems());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
}
